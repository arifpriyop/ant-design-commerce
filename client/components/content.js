import React from 'react';
import './content.css';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';

class ContentImg extends React.Component {
    container(props){
    }
    render(){
        const styles ={
            height: this.props.heightImg,
            borderStyle: "solid",
            borderWidth: 1
        }
        return(
           <div className="card">
               <Link to="/detail">
                <img className="card-img" src="https://www.w3schools.com/howto/img_avatar2.png" alt="Avatar"/>
                <div className="container-content">
                    <h4><b>Jane Doe</b></h4> 
                    <p>Interior Designer</p> 
                </div>
               </Link>
            </div>
        )
    }

}

export default ContentImg;
