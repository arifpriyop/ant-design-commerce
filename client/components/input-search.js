import React from 'react';
import { Input, Icon } from 'antd';

class InputSearch extends React.Component {
 
  render() {
    return (
      <Input
        className="input-search"
        placeholder="Find here"
        prefix={<Icon type="search" />}
        
      />
    );
  }
}
export default InputSearch;