 import React from 'react';
import {Layout, Menu, Row, Breadcrumb, Icon, Col, Pagination, Select, FormItem} from 'antd';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';
import Page1 from './page/page1';
import Page2 from './page/page2';
import PageSearch from './page/page_search';
import InputSearch from './input-search';
import {LocaleProvider, locales} from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';


import MenuToolBar from './menu';

const {Header, Content, Footer} = Layout;

class App extends React.Component {
    render() {
        const styles = {
            width: 40,
            height: 20
        }
        return (
            <LocaleProvider locale={enUS}>
                <Router>
                    <Layout className="layout">
                        <div className="div-header">
                            <Header className="header">
                                <Row gutter={16}>
                                    <Col span={4}>
                                        <Link to="/">
                                            <img className="img-logo"
                                                 src="http://cdn.mysitemyway.com/etc-mysitemyway/icons/legacy-previews/icons/glossy-black-3d-buttons-icons-sports-hobbies/044178-glossy-black-3d-button-icon-sports-hobbies-art-palette1-sc44.png"
                                                 alt="Avatar"/>
                                        </Link>
                                    </Col>
                                    <Col span={12}>
                                        <InputSearch/>
                                    </Col>
                                    <Col span={6}>
                                        <MenuToolBar/>
                                    </Col>
                                </Row>
                            </Header>
                        </div>
                        <div  style={{padding: '0 10em', marginTop: 20}}>
                        <Route exact path="/" component={Page1}/>
                        <Route path="/detail" component={Page2}/>
                        <Route path="/search" component={PageSearch}/>
                        </div>
                        <Footer className="footer" style={{textAlign: 'center'}}>
                            Ant Design ©2016 Created by Ant UED
                        </Footer>
                    </Layout>
                </Router>
            </LocaleProvider>
        )
    }
}

export default App;
