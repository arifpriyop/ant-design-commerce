import { Menu,Modal, Icon,Tooltip } from 'antd';
import React from 'react';
import ModalForm from './modal'
import './menu.css';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class MenuToolBar extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      visible : false
    }
    this.openModal = this.openModal.bind(this);
    this.handleOk = this.handleOk.bind(this);
  }
  handleOk (e){
    this.setState({
      visible : false
    })
  }
  openModal (e){
    switch(e.key){
      case "1":
        this.setState ({
        visible: true
      });
      break;
      default:
        console.log("sanguan");
    }
    
  }
  handleClick () {
    this.setState({
      current: e.key,
    });
  }
  render() {
    return (
      <div>
        <Menu
            className="menu"
            theme="dark"
            mode="horizontal"
            onClick={this.openModal}
            style={{ lineHeight: '64px' }}
        >
            <Menu.Item className="ant-nav-item" key="1">
              <Tooltip placement="bottom" title="Email">
                <Icon  type="mail" />
              </Tooltip>
              </Menu.Item>
            <Menu.Item className="ant-nav-item" key="2">
              <Tooltip placement="bottom" title="Profile"><Icon type="user" /></Tooltip>
            </Menu.Item>
            <SubMenu className="sub-menu-group" title={<Icon type="setting" />}>
                <Menu.Item key="setting:1">Option 1</Menu.Item>
                <Menu.Item key="setting:2">Option 2</Menu.Item>
                <Menu.Item key="setting:3">Option 3</Menu.Item>
                <Menu.Item key="setting:4">Option 4</Menu.Item>
            </SubMenu>
            <Menu.Item className="ant-nav-item" key="3" >
              <Tooltip placement="bottom" title="LogIn"><Icon type="login" /></Tooltip>
            </Menu.Item>
        </Menu>
        <Modal
            title="Register"
            visible={this.state.visible}
            onOk={this.handleOk}
            >
            <ModalForm/>
        </Modal>
      </div>
    );
  }
}

export default MenuToolBar;