import React from 'react';
import {Breadcrumb} from 'antd';

class BreadcrumbContent extends React.Component {
    render() {
        return (
            <div className="header-breadcrumb">
                <Breadcrumb>
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                    <Breadcrumb.Item><a href="">Application Center</a></Breadcrumb.Item>
                    <Breadcrumb.Item><a href="">Application List</a></Breadcrumb.Item>
                    <Breadcrumb.Item>An Application</Breadcrumb.Item>
                </Breadcrumb>
            </div>
        )
    }
}

export default BreadcrumbContent
