import React from 'react';
import {Tabs} from 'antd';

const TabPane = Tabs.TabPane;

class TabPage extends React.Component {
    render() {
        return (
            <Tabs defaultActiveKey="1" type="card" tabBarStyle={{margin:0}}>
                <TabPane tab="Tab 1" key="1">
                    <div className="content-page">

                    </div>

                </TabPane>
                <TabPane tab="Tab 2" key="2">
                    <div className="content-page">

                    </div>
                </TabPane>
                <TabPane tab="Tab 3" key="3">
                    <div className="content-page">

                    </div>
                </TabPane>
            </Tabs>
        )
    }
}

export default TabPage;