import React from 'react';
import {Layout, Breadcrumb, Row, Col,Rate,Form, InputNumber,Button} from 'antd';
import TabPage from './page2_tab';
import Sider from './../sidebar';
import BreadCrumbContent from './../breadcrumb';
import ContentImg from './../content';
const {Content} = Layout;
const FormItem = Form.Item;

class Page2 extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        const formItemLayout = {
            labelCol: { span: 2 },
            wrapperCol: { span: 1 },
        };
        return (
            <Content>
                <BreadCrumbContent/>
                <div>
                    <Row gutter={16}>
                        <Col span={18} order={4}>
                            <Row >
                                <Col span={10}>
                                    <div className="img-thumb">
                                        <img
                                            src="http://demo2.ninethemes.net/handy/opencart/image/cache/catalog/products/7_1-500x500.jpg"
                                            alt="Avatar"/>
                                        <div>
                                            <div className="box"></div>
                                            <div className="box"></div>
                                            <div className="box"></div>
                                            <div className="box"></div>
                                        </div>
                                    </div>

                                </Col>
                                <Col span={14}>
                                    <div className="spek-product">
                                        <h4>Judul Nya</h4>
                                        <Rate allowHalf defaultValue={5} />
                                        <div className="divider"></div>
                                        <h1>Rp 2.100.000</h1>
                                        <span>Tersedia 10</span>
                                        <div style={{marginTop:20}}>
                                            <Form>
                                                <FormItem
                                                    {...formItemLayout}
                                                    label="Qty"
                                                >
                                                    <InputNumber
                                                        min={0}
                                                    />
                                                </FormItem>
                                            </Form>
                                        </div>
                                        <div className="divider"></div>
                                        <Rate allowHalf defaultValue={5} />
                                        <span>Reviews</span>
                                        <div className="divider"></div>
                                        <div className="button-order">
                                            <Button type="primary"  icon="check">Beli Sekarang</Button>
                                            <Button type="primary"  icon="shopping-cart">Add To Chart</Button>
                                        </div>

                                    </div>
                                </Col>
                            </Row>
                            <div style={{marginTop:10,marginBottom:20}}>
                                <TabPage/>
                            </div>
                        </Col>
                        <Col span={6} order={3}>
                            <div className="content-author">
                                <label>Author</label>
                                <div className="">
                                    <img className="img-author" src="https://www.w3schools.com/howto/img_avatar.png" alt="Avatar"/>
                                    <span>Yanto Basna</span>
                                </div>
                            </div>
                            <div className="divider"></div>
                            <div className="content-page">
                                <Sider/>
                            </div>
                        </Col>
                    </Row>
                </div>
                <div className="content-related">
                    <h3>Barang Terkait</h3>
                    <Row>
                        <Col span={4}>
                            <ContentImg />
                        </Col>
                        <Col span={4}>
                            <ContentImg />
                        </Col>
                        <Col span={4}>
                            <ContentImg />
                        </Col>
                        <Col span={4}>
                            <ContentImg />
                        </Col>
                        <Col span={4}>
                            <ContentImg />
                        </Col>
                        <Col span={4}>
                            <ContentImg />
                        </Col>
                    </Row>
                </div>
            </Content>
        )
    }
}

export default Page2;