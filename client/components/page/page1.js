import React from 'react';
import {Layout} from 'antd';
import CarouselImg from './../carousel';
import PageContent from './page1_content';

const {Content} = Layout;

class Page1 extends React.Component {
    render() {
        return (
            <div>
                <Content>
                    <CarouselImg/>
                </Content>
                <Content style={{ marginTop: 10}}>
                    <div style={{minHeight: 380}}>
                        <PageContent/>
                    </div>
                </Content>
            </div>
        )
    }
}

export default Page1;