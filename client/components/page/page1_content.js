import React from 'react';
import {Col, Row, Pagination, Select} from 'antd';
import ContentImg from './../content';
import Sider from './../sidebar';


class PageContent extends React.Component {
    render(){
        return (
            <Row gutter={6}>
                <Col span={6}>
                    <div className="content">
                        <Sider/>
                    </div>
                </Col>
                <Col span={18}>
                    <div className="content">
                        <div className="header-content">
                            <h4>Option</h4>
                            <form>
                                <div style={{float: "right"}}>
                                    <label>Urutkan : </label>
                                    <Select defaultValue="lucy" style={{width: 120}}>
                                        <Select.Option value="jack">Jack</Select.Option>
                                        <Select.Option value="lucy">Lucy</Select.Option>
                                        <Select.Option value="disabled" disabled>Disabled</Select.Option>
                                        <Select.Option value="Yiminghe">yiminghe</Select.Option>
                                    </Select>
                                </div>
                            </form>
                        </div>
                        <div className="divider"></div>
                        <Row>
                            <Col span={8}>
                                <ContentImg/>
                            </Col>
                            <Col span={8}>
                                <ContentImg/>
                            </Col>
                            <Col span={8}>
                                <ContentImg/>
                            </Col>
                            <Col span={8}>
                                <ContentImg/>
                            </Col>
                            <Col span={8}>
                                <ContentImg/>
                            </Col>
                        </Row>

                        <div className="pagination-content">
                            <Pagination showSizeChanger defaultCurrent={3} total={500}/>
                        </div>
                    </div>
                </Col>
            </Row>
        )
    }
}

export default PageContent;