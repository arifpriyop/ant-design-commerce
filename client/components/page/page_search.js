import React from 'react';
import {Layout, Row, Col, Select, Pagination} from 'antd';
import Sider from './../sidebar';
import BreadCrumbContent from './../breadcrumb';
import ContentImg from './../content';

const {Content} = Layout;

class PageSearch extends React.Component {
    render() {
        return (
            <Content>
                <BreadCrumbContent/>
                <div>
                    <div className="header-content">
                        <h4>Search By : Sanguan</h4>
                        <form>
                            <div style={{float: "right"}}>
                                <label>Urutkan : </label>
                                <Select defaultValue="lucy" style={{width: 120}}>
                                    <Option value="jack">Jack</Option>
                                    <Option value="lucy">Lucy</Option>
                                    <Option value="disabled" disabled>Disabled</Option>
                                    <Option value="Yiminghe">yiminghe</Option>
                                </Select>
                            </div>
                        </form>
                    </div>
                    <div className="divider"></div>
                    <Row gutter={6}>
                        <Col span={4}>
                            <Sider/>
                        </Col>
                        <Col span={20}>
                            <div className="content-related">
                                <Row gutter={6}>
                                    <Col span={6}>
                                        <ContentImg/>
                                    </Col>
                                    <Col span={6}>
                                        <ContentImg/>
                                    </Col>
                                    <Col span={6}>
                                        <ContentImg/>
                                    </Col>
                                    <Col span={6}>
                                        <ContentImg/>
                                    </Col>
                                    <Col span={6}>
                                        <ContentImg/>
                                    </Col>
                                    <Col span={6}>
                                        <ContentImg/>
                                    </Col>
                                    <Col span={6}>
                                        <ContentImg/>
                                    </Col>
                                    <Col span={6}>
                                        <ContentImg/>
                                    </Col>
                                    <Col span={6}>
                                        <ContentImg/>
                                    </Col>
                                    <Col span={6}>
                                        <ContentImg/>
                                    </Col>
                                    <Col span={6}>
                                        <ContentImg/>
                                    </Col>
                                    <Col span={6}>
                                        <ContentImg/>
                                    </Col>
                                    <Col span={6}>
                                        <ContentImg/>
                                    </Col>
                                </Row>
                                <div className="pagination-content">
                                    <Pagination showSizeChanger defaultCurrent={3} total={500}/>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Content>

        )
    }
}

export default PageSearch