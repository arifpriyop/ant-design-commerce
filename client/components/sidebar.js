import React from 'react';
import { Menu, Icon } from 'antd';
const SubMenu = Menu.SubMenu;
import {
    Link
} from 'react-router-dom';

class Sider extends React.Component {
  render() {
    return (
        <div className="sidebar-content">
            <h4>Kategori</h4>
            <Menu
            mode="vertical"

            >

            <Menu.Item key="1">
                <Link to="/search"><span>Option 1</span></Link>
            </Menu.Item>
            <Menu.Divider></Menu.Divider>
            <Menu.Item key="2">Option 1</Menu.Item>
            <Menu.Divider></Menu.Divider>
            <Menu.Item key="3">Option 1</Menu.Item>
            <Menu.Divider></Menu.Divider>
            <Menu.Item key="4">Option 1</Menu.Item>
            <Menu.Divider></Menu.Divider>
            <Menu.Item key="5">Option 1</Menu.Item>
            <Menu.Divider></Menu.Divider>
            </Menu>
      </div>
    );
  }
}

export default Sider;